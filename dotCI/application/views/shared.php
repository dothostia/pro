

<div class="hero_bg shared_bg">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <h1 class="hero_title">SHARED CLOUD WEB HOSTING - CANADA</h1>
            <p class="hero_lead">93% of Canadian customers recommend our cloud web hosting as excellent with zero down time! </p>
            <a href="#" class="pull-right hero_action">Join our big family today</a>
         </div>
      </div>
   </div>
</div>
<div id="plans" class="block gray">
   <div class="container">
      <div class="row">
         <div id="shared">
            <div class="col-lg-3">
               <div class="box_shadow">
                  <div class="box_shadow_desc">
                     <h2 class="plan_name">Economic</h2>
                     <ul class="list-unstyled">
                        <li>Free domain registration</li>
                        <li>100 GB Disk Space</li>
                        <li>Unlimited Bandwidth</li>
                        <li>Unlimited Databases</li>
                        <li>50 Email Accounts</li>
                     </ul>
                  </div>
                  <div class="price">
                     <del>$3.95/month*</del>
                     <p class="current_price"><strong>$2.95<i>/month</i></strong></p>
                     <a href="#" class="btn btn-success" style="width:100%;">Get Started</a>
                  </div>
               </div>
            </div>
         </div>
         <div id="shared">
            <div class="col-lg-3">
               <div class="box_shadow">
                  <div class="box_shadow_desc">
                     <h2 class="plan_name">Startup Plus</h2>
                     <ul class="list-unstyled">
                        <li>Free domain registration</li>
                        <li>Free Comodo Positive SSL</li>
                        <li>150 GB Disk Space</li>
                        <li>Unlimited Bandwidth</li>
                        <li>Unlimited Databases</li>
                        <li>100 Email Accounts</li>
                     </ul>
                  </div>
                  <div class="price">
                     <del>$5.95/month*</del>
                     <p class="current_price"><strong>$3.95<i>/month*</i></strong></p>
                     <a href="#" class="btn btn-success" style="width:100%;">Get Started</a>
                  </div>
               </div>
            </div>
         </div>
         <div id="shared">
            <div class="col-lg-3">
               <div class="box_shadow">
                  <div class="box_shadow_desc">
                     <h2 class="plan_name">Business Pro</h2>
                     <ul class="list-unstyled">
                        <li>Free domain registration</li>
                        <li>Free Comodo Positive SSL</li>
                        <li>Unlimited Disk Space</li>
                        <li>Unlimited Bandwidth</li>
                        <li>Unlimited Databases</li>
                        <li>250 Email Accounts</li>
                     </ul>
                  </div>
                  <div class="price">
                     <del>$6.95/month*</del>
                     <p class="current_price"><strong>5.95<i>/month*</i></strong></p>
                     <a href="#" class="btn btn-success" style="width:100%;">Get Started</a>
                  </div>
               </div>
            </div>
         </div>
         <div id="shared">
            <div class="col-lg-3">
               <div class="box_shadow">
                  <div class="box_shadow_desc">
                     <h2 class="plan_name">SuperNatural</h2>
                     <ul class="list-unstyled">
                        <li>Business Pro features, plus</li>
                        <li>Free SiteLock Detect</li>
                        <li>Free domain name privacy</li>
                        <li>Free couple dedicated IP</li>
                        <li>Email Marketing (1000 contacts)</li>
                     </ul>
                  </div>
                  <div class="price">
                     <del>$9.95/month*</del>
                     <p class="current_price"><strong>7.95<i>/month*</i></strong></p>
                     <a href="#" class="btn btn-success" style="width:100%;">Get Started</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="cloudbg">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <div class="text-center">
               <h2 class="gadgets_title">What's behind the our fast cloud</h2>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="include_gadgets">
   <div class="container">
      <div class="row">
         <div class="col-lg-4">
            <div class="gadget_item">
               <img alt="24/7 canadian web hosting support" src="<?= base_url(); ?>public/img/support.png" alt="the best canadian web hosting support 24/7 based on Montreal Canada">
               <div>
                  <h3 class="gadget_title">Support that never sleeps</h3>
                  <p class="gadget_desc">
                     Other hosting providers take a vacation. We do not do it, our support work 24/7 to guarantee you the success and peace of mind.
                  </p>
               </div>
            </div>
         </div>
         <div class="col-lg-4">
            <div class="gadget_item">
               <img alt="best candian web hosting network" src="<?= base_url(); ?>public/img/rocket.png" alt="Quick Canadian web hosting based on Canada Montreal">
               <div>
                  <h3 class="gadget_title">High performance network</h3>
                  <p class="gadget_desc">
                     Also we hate downtimes. The availability of your website is our top priority for the development of your projects and your ideas.
                  </p>
               </div>
            </div>
         </div>
         <div class="col-lg-4">
            <div class="gadget_item">
               <img alt="candian ssd web hosting" src="<?= base_url(); ?>public/img/ssd.png" alt="Canadian SSD cloud hosting based on Montreal Quebec Canada">
               <div>
                  <h3 class="gadget_title">Cloud SSD Hosting</h3>
                  <p class="gadget_desc">
                     SSDs disks are used to speed up your website as you can communicate more data at the Input and output and avoid downtimes and bugs.
                  </p>
               </div>
            </div>
         </div>
         <div class="col-lg-4">
            <div class="gadget_item">
               <img alt="canada web hosting tools git ssh and more" src="<?= base_url(); ?>public/img/coding.png" alt="">
               <div>
                  <h3 class="gadget_title">Development Tools</h3>
                  <p class="gadget_desc">
                     We provide additional tools to developers because we like to see them big. Discover NodeJs, GIT, SSH and much more tools.
                  </p>
               </div>
            </div>
         </div>
         <div class="col-lg-4">
            <div class="gadget_item">
               <img alt="canada cloud web hosting fast and reliable" src="<?= base_url(); ?>public/img/cloud.png" alt="">
               <div>
                  <h3 class="gadget_title">Cloud Platform</h3>
                  <p class="gadget_desc">
                     We have servers located in America guarantee you fast, secure and reliable access. Discover our Data Centers, check our max speed zone.
                  </p>
               </div>
            </div>
         </div>
         <div class="col-lg-4">
            <div class="gadget_item">
               <img alt="canada web hosting security and guarantee" src="<?= base_url(); ?>public/img/secure.png" alt="">
               <div>
                  <h3 class="gadget_title">Security Guarantee</h3>
                  <p class="gadget_desc">
                     We have smart and robust tools that can handle massive attacks and migrate DDOS and other attacks and bad bots.
                  </p>
               </div>
            </div>
         </div>
         <div class="col-lg-4">
            <div class="gadget_item">
               <img alt="canada web hosting tools git ssh and more" src="<?= base_url(); ?>public/img/coding.png" alt="">
               <div>
                  <h3 class="gadget_title">Development Tools</h3>
                  <p class="gadget_desc">
                     We provide additional tools to developers because we like to see them big. Discover NodeJs, GIT, SSH and much more tools.
                  </p>
               </div>
            </div>
         </div>
         <div class="col-lg-4">
            <div class="gadget_item">
               <img alt="canada cloud web hosting fast and reliable" src="<?= base_url(); ?>public/img/cloud.png" alt="">
               <div>
                  <h3 class="gadget_title">Cloud Platform</h3>
                  <p class="gadget_desc">
                     We have servers located in America guarantee you fast, secure and reliable access. Discover our Data Centers, check our max speed zone.
                  </p>
               </div>
            </div>
         </div>
         <div class="col-lg-4">
            <div class="gadget_item">
               <img alt="canada web hosting security and guarantee" src="<?= base_url(); ?>public/img/secure.png" alt="">
               <div>
                  <h3 class="gadget_title">Security Guarantee</h3>
                  <p class="gadget_desc">
                     We have smart and robust tools that can handle massive attacks and migrate DDOS and other attacks and bad bots.
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="green_divider">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <p>
               Welcome to Dothostia, we are a web hosting company in Canada that offers different hosting ranges including shared hosting, reseller hosting, VPS and dedicated servers as well as domain names and ssl certificates. With more than 7000 customers, we have a reputation for excellence and we focus on excellent and reliable service levels and customer support. This means that you can use and enjoy our hosting services if you are a freelancer at home, a small, medium or large business, we are sure to have a hosting solution for you.
            </p>
         </div>
      </div>
   </div>
</div>
<div id="sections">
   <div class="block">
      <div class="container">
         <div class="row">
            <div class="col-lg-6">
               <img src="<?= base_url(); ?>public/img/apps-icons.png" alt="the best canada wordpress hosting width free domain name, based on Montreal Quebec" class="img-responsive text-center">
            </div>
            <div class="col-lg-6">
               <h3>Your Presence in Minutes</h3>
               <p>
                  Install your WordPress blog, Joomla CMS, phpBB forums, Magento and more than 300 other popular apps with just 1 single click. Softaculous allows you to install, update and manage web applications without the need for technical or prior knowledge.
               </p>
               <p>
                  It is completely free to use it and there is no limit on the number of scripts you can have, just in one click you will have an online presence.
               </p>
            </div>
         </div>
      </div>
   </div>
   <div class="block gray">
      <div class="container">
         <div class="row">
            <div class="col-lg-6">
               <h3>Fast Data Readings (SSD)</h3>
               <p>
                  We only use enterprise class SSDs on all shared hosting servers for the most optimal performance.
               </p>
               <p>
                  The SSD uses flash memory to store data, which provides the best performance and reliability on a hard drive. SSD generates less heat, helping to increase its life and reliability.
               </p>
            </div>
            <div class="col-lg-6">
               <div class="text-center">
                  <img alt="the best canadian web hosting with powered by SSD servers, based on Montreal Quebec" src="https://my-vm.com/wp-content/uploads/2016/11/my-vm-hdd-ssd.png" class="img-responsive">
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="block">
      <div class="container">
         <div class="row">
            <div class="col-lg-6">
               <img alt="canada cpanel web hosting cheap and with free domain name, based on Montreal Quebec" src="<?= base_url(); ?>public/img/paper-lantern.gif" class="img-responsive">
            </div>
            <div class="col-lg-6">
               <h3>Powered by cPanel Mobile-UI</h3>
               <p>
                  Easily manage your website with the CPanel control panel that we provide for free with the purchase of a web hosting package from Dothostia. It allows you to manage your database, access your personalized mailboxes, install software such as wordpress, prestashop ... with Softaculous, send a support ticket to our team and much more
               </p>
               <p>
                  Dothostia offers its customers the ability to create hundreds of subdomains at no additional cost from their CPanel interface as well as the creation of professional email accounts
               </p>
            </div>
         </div>
      </div>
   </div>
   <div class="block gray">
      <div class="container">
         <div class="row">
            <div class="col-lg-6">
               <h3>Availability 99.9 Cloud & Secure</h3>
               <p>
                  With our established cloud infrastructure reaching worldwide, we are confident that we can maintain 99.90% guaranteed availability for our clients interested in shared web hosting in Canada and outside of Canada.
               </p>
               <p>
                  In Dothostia we are proud to offer different types of high quality web hosting enhanced by 24/7 support. We use only the most reliable data centers that have redundant power, on-site diesel generators and backup batteries, as well as multiple network transit paths.
               </p>
            </div>
            <div class="col-lg-6">
               <img alt="the most secure canadian web hosting based on Montreal Quebec" src="<?= base_url(); ?>public/img/securityhosting.png" class="img-responsive" alt="">
            </div>
         </div>
      </div>
   </div>
   <div class="block">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <h3>All our plans include</h3>
            </div>
            <div class="col-lg-4">
               <ul class="list-unstyled specs">
                  <li><i class="fa fa-chevron-right">&nbsp;</i> Centralized Product Management</li>
                  <li><i class="fa fa-chevron-right">&nbsp;</i> 24/7 support</li>
                  <li><i class="fa fa-chevron-right">&nbsp;</i> Guaranteed service 30 days or money back</li>
                  <li><i class="fa fa-chevron-right">&nbsp;</i> Free initial configuration</li>
                  <li><i class="fa fa-chevron-right">&nbsp;</i> Access to our reward program</li>
               </ul>
            </div>
            <div class="col-lg-4">
               <ul class="list-unstyled specs">
                  <li><i class="fa fa-chevron-right">&nbsp;</i> Softaculous software</li>
                  <li><i class="fa fa-chevron-right">&nbsp;</i> Cloud Architecture</li>
                  <li><i class="fa fa-chevron-right">&nbsp;</i> CPanel® access</li>
                  <li><i class="fa fa-chevron-right">&nbsp;</i> High performance network</li>
                  <li><i class="fa fa-chevron-right">&nbsp;</i> Saved backups 7 days / Amazon S3</li>
               </ul>
            </div>
            <div class="col-lg-4">
               <ul class="list-unstyled specs">
                  <li><i class="fa fa-chevron-right">&nbsp;</i> PHP 5.2 à 5.6</li>
                  <li><i class="fa fa-chevron-right">&nbsp;</i> Ruby 1.8 à 2.1</li>
                  <li><i class="fa fa-chevron-right">&nbsp;</i> Python 2.7, 3.3 & 3.4</li>
                  <li><i class="fa fa-chevron-right">&nbsp;</i> MySQL (MariaDB), PostgreSQL</li>
                  <li><i class="fa fa-chevron-right">&nbsp;</i> Web Statistics & Access Logs</li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <div class="block gray">
      <div id="faq">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <div class="text-center">
                     <h2 class="faq-title">Frequently Asked Questions</h2>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="faq_block">
                     <h5><strong>What is web hosting?</strong></h5>
                     <p>
                        Web hosting is an Internet service for storing your web site files online, so that other people can search and find your site. Morocco web hosting is available in several flavors, including VPS servers, content distribution (CDN), shared and dedicated hosting.
                     </p>
                  </div>
                  <div class="faq_block">
                     <h5><strong>Why is web hosting so important?</strong></h5>
                     <p>
                        Let's say you want to build a website for your project to promote your products and / or services to customers. One of the first things you will need is a web host like Dothostia to host your site once it is built.
                     </p>
                  </div>
                  <div class="faq_block">
                     <h5><strong>What is the right type of web hosting for you?</strong></h5>
                     <p>
                        Whether you are a blogger, an SME, or an e-commerce ... Dothostia has the best web hosting solution for you. But before choosing a hosting plan, please contact our help service via LiveChat, to help you choose the plan that suits you.
                     </p>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="faq_block">
                     <h5><strong>How to transfer my account to Dothostia?</strong></h5>
                     <p>
                        It's easy to transfer your site to your new hosting account at Dothostia. Our specialists are ready to help you transfer all your files, databases, domain names, e-mails and we will ensure that everything works after the transfer.
                     </p>
                  </div>
                  <div class="faq_block">
                     <h5><strong>What are the payment methods do you accept?</strong></h5>
                     <p>
                        We accept all MasterCard, Visa, Paypal, Discover Network, American Express credit cards as well as bank deposits or wafacash and Western Union and of course cash in our offices.
                     </p>
                  </div>
                  <div class="faq_block">
                     <h5><strong>How long does it take to set up my account?</strong></h5>
                     <p>
                        After purchasing your hosting package, your account begins the installation process. The estimated time for each type of accommodation is as follows:
                        <ul class="list-unstyled">
                           <li>
                           <i class="fa green fa-chevron-circle-down"></i> <a href="https://www.dothostia.com/fr/hebergement-web-maroc.html">Shared Web Hosting</a> ou <a href="https://www.dothostia.com/fr/hebergement-web-revendeurs.html">Reseller Hosting</a> : 1H Maximum</li>
                           <li><i class="fa green fa-chevron-circle-down"></i> <a href="https://www.dothostia.com/serveurs-vps-maroc.html">cPanel VPS Server</a> : 3H Maximum</li>
                           <li><i class="fa green fa-chevron-circle-down"></i> <a href="https://www.dothostia.com/serveurs-dedies-maroc.html">Dedicated Server</a> : 48h Maximum</li>
                           <li><i class="fa green fa-chevron-circle-down"></i> <a href="https://www.dothostia.com/fr/certificat-ssl-maroc.html">SSL Certificate</a> : 30 minutes Maximum</li>
                        </ul>
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="footer_hero">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <h2 class="footer_lead">We support you every step of the way</h2>
               <p><strong>Make your website faster and enjoy the best support.</strong></p>
               <a href="#" class="btn btn-success">Get Started Right Now</a>
            </div>
         </div>
      </div>
   </div>
</div>

