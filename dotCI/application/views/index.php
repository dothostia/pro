<div class="hero_bg index_bg">
               <div class="vline">
               </div>
               <div class="teaser">
                  <span>Ahoy hoy...</span>
                  <span><strong>Up to 40% off</strong></span>
                  <span>+ Domaine Offert</span>
                  <span>+ Certificat SSL Offert</span>
               </div>
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12 col-xs-12">
                        <h1>Your Affordable Web Hosting with</h1>
                        <h3><span>Unlimeted</span> space</h3>
                        <div class="block">
                           <p>
                              93% of our clients recommend our cloud web hosting as the best in Canada<br>
                              <a href="<?= base_url(); ?>canada/web-hosting" class="action"> Join our growing family of Canadian customers!</a>
                           </p>
                        </div>
                        <div class="row">
                           <div class="col-lg-4">
                              <a href="#" class="btn_banner">Join our big family today &#10084;</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="plans" class="block">
               <div class="container">
                  <div class="row">
                     <div id="home_plans">
                        <div class="col-lg-3 home_plans">
                           <h3 class="title">Shared Web Hosting</h3>
                           <div class="icon_inner">
                              <i class="fa fa-users"></i>
                           </div>
                           <div class="plan_desc">
                              <p>starting from <strong>249 DH </strong></p>
                              <p>
                                 An online presence is guaranteed with unlimited disk space and bandwidth plus a free domain and SSL.
                              </p>
                              <p>
                                 <a href="#" class="btn btn-success">READ MORE</a>
                              </p>
                           </div>
                        </div>
                        <hr class="visible-xs">
                        <div class="col-lg-3 home_plans">
                           <h3 class="title">Reseller Hosting</h3>
                           <div class="icon_inner">
                              <i class="fa fa-users"></i>
                           </div>
                           <div class="plan_desc">
                              <p>starting from <strong>249 DH </strong></p>
                              <p>
                                 Host multiple sites under one account. Powered by cPanel / WHM, while getting a white label mark
                              </p>
                              <p>
                                 <a href="#" class="btn btn-success">READ MORE</a>
                              </p>
                           </div>
                        </div>
                        <hr class="visible-xs">
                        <div class="col-lg-3 home_plans">
                           <h3 class="title">VPS HOSTING</h3>
                           <div class="icon_inner">
                              <i class="fa fa-users"></i>
                           </div>
                           <div class="plan_desc">
                              <p>starting from <strong>249 DH </strong></p>
                              <p>
                                  High performance for you. Increase your speed, uptime, and security with our fully managed SSD VPS
                              </p>
                              <p>
                                 <a href="#" class="btn btn-success">READ MORE</a>
                              </p>
                           </div>
                        </div>
                        <hr class="visible-xs">
                        <div class="col-lg-3 home_plans">
                           <h3 class="title">Dedicated Hosting</h3>
                           <div class="icon_inner">
                              <i class="fa fa-users"></i>
                           </div>
                           <div class="plan_desc">
                              <p>starting from <strong>249 DH </strong></p>
                              <p>
                                 Have a dedicated server and get a high performance hardware and a lot more power and space.
                              </p>
                              <p>
                                 <a href="#" class="btn btn-success">READ MORE</a>
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="sections">
               <div class="block gray">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-12 text-center">
                           <h2>We are proud of our support to our customer 24/7</h2>
                           <p>
                              What is web hosting without a solid support infrastructure? In Dothostia we have an experienced technical support team working around the clock 365 days a year to ensure your website is up and running smoothly. Our cloud infrastructure ensures the speed and security of your website.
                           </p>
                        </div>
                        <div class="col-lg-4 text-center">
                           <img src="<?= base_url(); ?>public/img/icon_phone.png">
                           <h4>Reply in 15 seconds</h4>
                           <p>
                              Now do not wait - we will answer you in just 15 seconds. If you have a question or need help please talk to our friendly team at Live Chat. We are here to help you.
                           </p>
                        </div>
                        <div class="col-lg-4 text-center">
                           <img src="<?= base_url(); ?>public/img/icon_money.png">
                           <h4>30 Days Satisfied or Refunded</h4>
                           <p>
                              We are so confident in the quality of our services that guarantee your satisfaction. If for any reason you are not satisfied, cancel all and get a full refund within 30 days.
                           </p>
                        </div>
                        <div class="col-lg-4 text-center">
                           <img src="<?= base_url(); ?>public/img/icon_servers_up-arrow.png">
                           <h4>Availability guarantee 365 / year</h4>
                           <p>
                              In the event of a hardware failure (which almost never happens), a second server takes over and backups are copied every 24 hours to an external server to protect your data. 
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="block">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-6">
                           <img style="max-height:none !important;" src="<?= base_url(); ?>public/img/Sans-titre.png" class="img-responsive">
                        </div>
                        <div class="col-lg-6">
                           <h3>Canadian Standard Web Hosting</h3>
                           <p>
                              At Dothostia, we specialize in low cost hosting services. If you are looking for a Canadian cloud hosting that allows the registration of domain names also, look no further! Our cloud hosting services are thought to be easy to use. All of our plans include disk space and unmeasured broadband, an unlimited number of hosted domains, subdomains, MySQL databases, FTP and email accounts. This means that you can use and enjoy our hosting services if you are a home user, a developer, a small or a large company in Canada
                           </p>
                           <h3>Infrastructure without worries</h3>
                           <p>
                              Manage your invoices and products with ease with our unique and scalable customer interface, where efficiency meets aestheticsque
                           </p>
                           <p>
                              <div class="row">
                                 <div class="col-lg-6">
                                    <ul class="list-unstyled">
                                       <li><i class="far fa-clock"></i> Responsive Plateform</li>
                                       <li><i class="fas fa-wrench"></i> Updates, news...</li>
                                       <li><i class="fas fa-globe-americas"></i> Easy domains management</li>
                                    </ul>
                                 </div>
                                 <div class="col-lg-6">
                                    <ul class="list-unstyled">
                                       <li><i class="fas fa-file-invoice"></i> Comprehensible billing</li>
                                       <li><i class="fab fa-product-hunt"></i> Centralized Product Management</li>
                                       <li><i class="far fa-life-ring"></i> Online Support Center</li>
                                    </ul>
                                 </div>
                              </div>
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>