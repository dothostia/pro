<div class="hero_bg dedicated_bg">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <h1 class="hero_title">HÉBERGEMENT WEB MAROC - CLOUD</h1>
                        <p class="hero_lead">93% des clients marocains recommandent notre hébergement web mutualisé comme excellent !</p>
                        <a href="#" class="pull-right hero_action">Joignez-vous à notre famille</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="hero_specs">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-4">
                        <div class="row">
                           <div class="col-lg-2"><i class="far fa-life-ring"></i></div>
                           <div class="col-lg-10">
                              <h5>FULLY MANAGED</h5>
                              <p>
                                 VPS Linux pas cher ainsi qu'un environnement souple pour vos projets les plus exigeants.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="row">
                           <div class="col-lg-2"><i class="far fa-thumbs-up"></i></div>
                           <div class="col-lg-10">
                              <h5>ZERO DOWNTIME SLA</h5>
                              <p>
                                 VPS Linux pas cher ainsi qu'un environnement souple pour vos projets les plus exigeants.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="row">
                           <div class="col-lg-2"><i class="fab fa-cloudversify"></i></div>
                           <div class="col-lg-10">
                              <h5>BASED ON THE CLOUD</h5>
                              <p>
                                 VPS Linux pas cher ainsi qu'un environnement souple pour vos projets les plus exigeants.
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="description">
               <div class="container">
                  <div class="block">
                     <div class="row">
                        <div class="col-lg-8">
                           <h3>Solution for Managing Your Server</h3>
                           <p>
                              All of Dothostia's dedicated servers come with the best quality criteria and bandwidth quotas, all for a single monthly cost. Our servers benefit from free installation, and they are deployed in a few hours. As an added benefit, all customers can take advantage of our free transfer service, which means that we will transfer together your existing sites from your current host to a new server with us - for free!
                           </p>
                           <p>
                              At Dothostia, we are proud of our service and support. Our servers are properly housed in state-of-the-art data centers that are busy 24/7. For your peace of mind, we offer a 100% network uptime guarantee.
                           </p>
                        </div>
                        <div class="col-lg-4">
                           <h3>Caractéristiques</h3>
                           <p>
                              <ul class="details list-unstyled specs">
                                 <li><span><i class="fa fa-chevron-right"></i></span> Centralized Product Management</li>
                                 <li><span><i class="fa fa-chevron-right"></i></span> Support 24/7</li>
                                 <li><span><i class="fa fa-chevron-right"></i></span> Guaranteed service 30 days or money back</li>
                                 <li><span><i class="fa fa-chevron-right"></i></span> Free initial configuration</li>
                              </ul>
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="plans" class="block gray">
               <div class="container">
                  <div class="row">
                     <div id="vps">
                        <div class="col-lg-3">
                           <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Economique</h2>
                              <ul class="list-unstyled">
                                 <li>1 Domaine gratuit à vie</li>
                                 <li>20 Go d'espace disque</li>
                                 <li>250 Go de bande passante</li>
                                 <li>50 comptes courriel</li>
                                 <li>10 bases de données</li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>249 dh <i>&nbsp;/an</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Commander</a>
                           </div>
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Economique</h2>
                              <ul class="list-unstyled">
                                 <li>1 Domaine gratuit à vie</li>
                                 <li>20 Go d'espace disque</li>
                                 <li>250 Go de bande passante</li>
                                 <li>50 comptes courriel</li>
                                 <li>10 bases de données</li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>249 dh <i>&nbsp;/an</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Commander</a>
                           </div>
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Economique</h2>
                              <ul class="list-unstyled">
                                 <li>1 Domaine gratuit à vie</li>
                                 <li>20 Go d'espace disque</li>
                                 <li>250 Go de bande passante</li>
                                 <li>50 comptes courriel</li>
                                 <li>10 bases de données</li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>249 dh <i>&nbsp;/an</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Commander</a>
                           </div>
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Economique</h2>
                              <ul class="list-unstyled">
                                 <li>1 Domaine gratuit à vie</li>
                                 <li>20 Go d'espace disque</li>
                                 <li>250 Go de bande passante</li>
                                 <li>50 comptes courriel</li>
                                 <li>10 bases de données</li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>249 dh <i>&nbsp;/an</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Commander</a>
                           </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="cloudbg">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="text-center">
                           <h2 class="gadgets_title">Qu'est ce qu'il y a derrière le Cloud</h2>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="include_gadgets">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 No experience? no problem. We will monitor, secure  and maintain your VPS. Our support work around the clock to help you.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 The most popular distribution, CentOS is an open source platform. A business-grade solution without any commercial license.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 A flexible control panel focuses on usability, streamlining the complexity of traditional web hosting.
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 We provide extra storage space for the back-up, and extra bandwidth in time of need at a minimum cost for much more space.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 Other web hosts take vacations, we do not do it. Our support work 24/7 via livechat or via a phone call to help you.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 OnApp and cPanel combine to offer a robust VPS. Use OnApp to start, stop, restart, and control your VPS.
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 Start with 4GB of RAM and easily scale your VPS server up to 8GB from your OnApp control panel or per ticket.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 SSDs improve the performance of your VPS and become faster and more efficient at I / O inputs and outputs.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 We deliver VPS from our infrastructure based on Montreal Canada, which means we offer a superb VPS hosting experience.
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="sections">
               <div class="block gray">
                  <div id="faq">
                     <div class="container">
                        <div class="row">
                           <div class="col-lg-12">
                              <div class="text-center">
                                 <h2 class="faq-title">Questions Fréquemment Posées</h2>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="faq_block">
                                 <h5><strong>Je vais être capable de gérer mon serveur via un panneau de contrôle basé sur le Web?</strong></h5>
                                 <p>
                                    Oui. Chaque serveur dédié est livré avec Web Host Manager (WHM), qui vous donne un contrôle complet sur la création et la personnalisation de votre compte, ainsi que la gestion de tous les aspects de votre serveur.
                                 </p>
                              </div>
                              <div class="faq_block">
                                 <h5><strong>Je vais être capable de gérer mon serveur via un panneau de contrôle basé sur le Web?</strong></h5>
                                 <p>
                                    Oui. Chaque serveur dédié est livré avec Web Host Manager (WHM), qui vous donne un contrôle complet sur la création et la personnalisation de votre compte, ainsi que la gestion de tous les aspects de votre serveur.
                                 </p>
                              </div>
                              <div class="faq_block">
                                 <h5><strong>Je vais être capable de gérer mon serveur via un panneau de contrôle basé sur le Web?</strong></h5>
                                 <p>
                                    Oui. Chaque serveur dédié est livré avec Web Host Manager (WHM), qui vous donne un contrôle complet sur la création et la personnalisation de votre compte, ainsi que la gestion de tous les aspects de votre serveur.
                                 </p>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="faq_block">
                                 <h5><strong>Je vais être capable de gérer mon serveur via un panneau de contrôle basé sur le Web?</strong></h5>
                                 <p>
                                    Oui. Chaque serveur dédié est livré avec Web Host Manager (WHM), qui vous donne un contrôle complet sur la création et la personnalisation de votre compte, ainsi que la gestion de tous les aspects de votre serveur.
                                 </p>
                              </div>
                              <div class="faq_block">
                                 <h5><strong>Je vais être capable de gérer mon serveur via un panneau de contrôle basé sur le Web?</strong></h5>
                                 <p>
                                    Oui. Chaque serveur dédié est livré avec Web Host Manager (WHM), qui vous donne un contrôle complet sur la création et la personnalisation de votre compte, ainsi que la gestion de tous les aspects de votre serveur.
                                 </p>
                              </div>
                              <div class="faq_block">
                                 <h5><strong>Je vais être capable de gérer mon serveur via un panneau de contrôle basé sur le Web?</strong></h5>
                                 <p>
                                    Oui. Chaque serveur dédié est livré avec Web Host Manager (WHM), qui vous donne un contrôle complet sur la création et la personnalisation de votre compte, ainsi que la gestion de tous les aspects de votre serveur.
                                 </p>
                              </div>
                           </div>
                        </div>
                  </div>
                  </div>
               </div>
               <div class="footer_hero">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-12">
                           <h2 class="footer_lead">Nous vous supportons à chaque étape</h2>
                           <p><strong>Faites de votre site plus rapide et profiter du meilleur soutien.</strong></p>
                           <a href="#" class="btn btn-success">Commencer dès maintenant</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>