<div class="hero_bg vps_bg">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <h1 class="hero_title">VPS HOSTING - CANADA</h1>
                        <p class="hero_lead">93% of Canadian customers recommend our cloud web hosting as excellent with zero down time! </p>
                        <a href="#" class="pull-right hero_action">Join our big family today</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="hero_specs">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-4">
                        <div class="row">
                           <div class="col-lg-2"><i class="far fa-life-ring"></i></div>
                           <div class="col-lg-10">
                              <h5>FULLY MANAGED & FREE CPANEL</h5>
                              <p>
                                 OS security patches and updates, cPanel/WHM updates, and more.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="row">
                           <div class="col-lg-2"><i class="far fa-thumbs-up"></i></div>
                           <div class="col-lg-10">
                              <h5>ZERO DOWNTIME SLA</h5>
                              <p>
                                 Our VPS servers work as dedicated servers. Fast, reliable and efficient.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="row">
                           <div class="col-lg-2"><i class="fab fa-cloudversify"></i></div>
                           <div class="col-lg-10">
                              <h5>BASED ON THE CLOUD</h5>
                              <p>
                                 Our redundant hardware clusters mean virtually no downtime.
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="plans" class="block gray">
               <div class="container">
                  <div class="row">
                     <div id="vps">
                        <div class="col-lg-3">
                           <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Quickbee</h2>
                              <ul class="list-unstyled">
                                 <li><b>4 VCores</b> (CPU)</li>
                                 <li><b>4GB</b> Ram memory</li>
                                 <li><b>100GB</b> SSD Disk space</li>
                                 <li><b>4TB</b> Bandwidth</li>
                                 <li>Fully Managed w/cPanel</li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>$41.94<i>/month</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Get Started</a>
                           </div>
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Lovebeat</h2>
                              <ul class="list-unstyled">
                                 <li><b>8 VCores</b> (CPU)</li>
                                 <li><b>8GB</b> Ram memory</li>
                                 <li><b>200GB</b> SSD Disk space</li>
                                 <li><b>8TB</b> Bandwidth</li>
                                 <li>Fully Managed w/cPanel</li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>$72.24<i>/month</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Get Started</a>
                           </div>
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Wildhoney</h2>
                              <ul class="list-unstyled">
                                 <li><b>12 VCores</b> (CPU)</li>
                                 <li><b>12GB</b> Ram memory</li>
                                 <li><b>250GB</b> SSD Disk space</li>
                                 <li><b>12TB</b> Bandwidth</li>
                                 <li>Fully Managed w/cPanel</li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>$119.64<i>/month</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Get Started</a>
                           </div>
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Watercircle</h2>
                              <ul class="list-unstyled">
                                 <li><b>16 VCores</b> (CPU)</li>
                                 <li><b>24GB</b> Ram memory</li>
                                 <li><b>500GB</b> SSD Disk space</li>
                                 <li><b>16TB</b> Bandwidth</li>
                                 <li>Fully Managed w/cPanel</li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>$179.84<i>/month</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Get Started</a>
                           </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="cloudbg">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="text-center">
                           <h2 class="gadgets_title">Qu'est ce qu'il y a derrière le Cloud</h2>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="include_gadgets">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 No experience? no problem. We will monitor, secure  and maintain your VPS. Our support work around the clock to help you.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 The most popular distribution, CentOS is an open source platform. A business-grade solution without any commercial license.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 A flexible control panel focuses on usability, streamlining the complexity of traditional web hosting.
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 We provide extra storage space for the back-up, and extra bandwidth in time of need at a minimum cost for much more space.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 Other web hosts take vacations, we do not do it. Our support work 24/7 via livechat or via a phone call to help you.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 OnApp and cPanel combine to offer a robust VPS. Use OnApp to start, stop, restart, and control your VPS.
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 Start with 4GB of RAM and easily scale your VPS server up to 8GB from your OnApp control panel or per ticket.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 SSDs improve the performance of your VPS and become faster and more efficient at I / O inputs and outputs.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 We deliver VPS from our infrastructure based on Montreal Canada, which means we offer a superb VPS hosting experience.
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="sections">
               <div class="block gray">
                  <div id="faq">
                     <div class="container">
                        <div class="row">
                           <div class="col-lg-12">
                              <div class="text-center">
                                 <h2 class="faq-title">Questions Fréquemment Posées</h2>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="faq_block">
                                 <h5><strong>How to choose the right VPS according to the traffic of my website?</strong></h5>
                                 <p>
                                    This is an opportunity to take advantage of our expertise! We are here to help you make the right decision based on your configuration, traffic, size of your databases and more. Do not hesitate to contact us before placing your order.
                                 </p>
                              </div>
                              <div class="faq_block">
                              <h5><strong>What control panel is installed on my VPS?</strong></h5>
                                 <p>
                                    Our VPS offer market standards with cPanel / WHM pre-installed with the Onapp cloud solution. Do not worry, you will be in a familiar environment.
                                 </p>
                              </div>
                              <div class="faq_block">
                                 <h5><strong>I will be able to manage my server via a web-based control panel?</strong></h5>
                                 <p>
Yes. Each dedicated server comes with Web Host Manager (WHM), which gives you complete control over creating and customizing your account, as well as managing all aspects of your server.
                                 </p>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="faq_block">
                                 <h5><strong>What is a VPS server?</strong></h5>
                                 <p>
                                    VPS stands for Virtual Private Server, VPS is a virtual machine that runs its own copy of an operating system such as Centos, Fedora, Ubuntu or Windows. VPS are therefore interesting solutions for those who need more than shared hosting.
                                 </p>
                              </div>
                              <div class="faq_block">
                                 <h5><strong>Who uses VPS hosting?</strong></h5>
                                 <p>
Many hosting customers have a preference for VPS hosting because they have more control over their environment than shared hosting. The VPS is a shared environment is just as appropriate, the control they enjoy is worth the extra expense. VPS hosting is good for developers and Startup as well as all SMEs.
                                 </p>
                              </div>
                              <div class="faq_block">
                                 <h5><strong>How to upgrade my VPS?</strong></h5>
                                 <p>
With your Onapp client interface, adjust the resources of your VPS simply! At any time, from your VPS management page select a new RAM / CPU range, or double your disk space with just one click.
                                 </p>
                              </div>
                           </div>
                        </div>
                  </div>
                  </div>
               </div>
               <div class="footer_hero">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-12">
                           <h2 class="footer_lead">We bring you every step of the way</h2>
                           <p><strong>Make your site faster and enjoy the best support.</strong></p>
                           <a href="#" class="btn btn-success">Get started right now</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>