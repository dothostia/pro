<!DOCTYPE html>
<html lang="en-CA">
   <head>
      <meta charset="utf-8">
      <title><?= $title; ?> | Domain names - Quick and secure cloud Hosting</title>
      <meta name="description" content="<?= $desc; ?>">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"> 
      <link rel="stylesheet" href="<?= base_url(); ?>public/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?= base_url(); ?>public/css/style.css">
      <script src="<?= base_url(); ?>public/js/jquery-3.3.1.min.js"></script>
      <script src="<?= base_url(); ?>public/js/bootstrap.min.js"></script>
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125397107-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-125397107-1');
      </script>
   </head>
   <body>
      <div id="main">
         <div id="header">
            <div class="top_header hidden-xs">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-6">
                        <ul class="social_links list-inline">
                           <li><a href="#"><i class="fab fa-facebook-f">&nbsp;</i></a></li>
                           <li><a href="#"><i class="fab fa-twitter">&nbsp;</i></a></li>
                           <li><a href="#"><i class="fab fa-google-plus-g">&nbsp;</i></a></li>
                           <li><a href="#"><i class="fab fa-linkedin-in">&nbsp;</i></a></li>
                           <li><a href="#"><i class="fab fa-youtube">&nbsp;</i></a></li>
                        </ul>
                     </div>
                     <div class="col-lg-6">
                        <ul class="top_nav list-inline">
                           <li><a href="#">About us</a></li>
                           <li><a href="#"><i class="fas fa-lock">&nbsp;</i> Client Login Area</a></li>
                           <li class="support"><a href="#"><i class="fas fa-headset">&nbsp;</i> Support </a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div class="main_header">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <nav class="navbar">
                           <div class="container-fluid">
                              <div class="navbar-header">
                                 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <i class="fas fa-bars" style="font-size:40px;"></i>
                                 </button>
                                 <a class="navbar-brand logo" href="<?= base_url(); ?>"><img alt="canadian web hosting" class="img-responsive" src="<?= base_url(); ?>public/img/logo.png"></a>
                              </div>
                              <div id="navbar" class="navbar-collapse collapse">
                                 <ul class="nav navbar-nav navbar-right">
                                    <li class="active"><a href="<?= base_url(); ?>canada/domain-name">Domains<span class="sr-only">(current)</span></a></li>
                                    <li><a href="#">Websites</a></li>
                                    <li class="dropdown">
                                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hosting <span class="caret"></span></a>
                                       <ul class="dropdown-menu">
                                          <li><a href="<?= base_url(); ?>canada/web-hosting">Shared Hosting</a></li>
                                          <li><a href="<?= base_url(); ?>canada/reseller-hosting">Reseller Hosting</a></li>
                                          <li><a href="<?= base_url(); ?>canada/vps-hosting">VPS Hosting</a></li>
                                          <li><a href="<?= base_url(); ?>canada/dedicated-server">Dedicated Hosting</a></li>
                                       </ul>
                                    </li>
                                    <li><a href="#">Office Emails</a></li>
                                    <li><a href="<?= base_url(); ?>canada/ssl-certificates">SECURITY</a></li>
                                 </ul>
                              </div>
                              <!--/.nav-collapse -->
                           </div>
                           <!--/.container-fluid -->
                        </nav>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="main_body">