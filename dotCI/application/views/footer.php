
            </div>
            <div id="testimonial">
               <div class="block gray">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-12">
                           <h2 class="capital_lead text-center">Our pride: thousands of satisfied customers!</h2>
                           <div class="clearfix"></div>
                           <div class="carousel">
                              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                 <!-- Indicators -->
                                 <ol class="carousel-indicators">
                                    <li data-target="#myCarousel" data-slide-to="0" class="active">&nbsp;</li>
                                    <li data-target="#myCarousel" data-slide-to="1">&nbsp;</li>
                                    <li data-target="#myCarousel" data-slide-to="2">&nbsp;</li>
                                 </ol>
                                 <!-- Wrapper for slides -->
                                 <div class="carousel-inner">
                                  <div class="item active">
                                    <div class="item-bg">
                                       <img src="<?= base_url(); ?>public/img/mehdi.jpg" alt="Chania">
                                    </div>
                                    <div class="carousel-caption">
                                      <div class="row">
                                         <div class="col-lg-6 col-lg-offset-3">
                                             <p>
                                               On est avec Dothostia pour près de 2 ans, leur équipe serviable et professionnelle. Au premier temps on a eu un hébergement mutualisé et maintenant on a passé au dernier…
                                             </p>
                                             <h5>Los Angeles</h5>
                                             <a href="#">www.dataxpress.ma</a>
                                         </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="item">
                                    <div class="item-bg">
                                       <img src="<?= base_url(); ?>public/img/mehdi.jpg" alt="Chania">
                                    </div>
                                    <div class="carousel-caption">
                                      <h3>Chicago</h3>
                                      <p>Thank you, Chicago!</p>
                                    </div>
                                  </div>
                                  <div class="item">
                                    <div class="item-bg">
                                       <img src="<?= base_url(); ?>public/img/mehdi.jpg" alt="Chania">
                                    </div>
                                    <div class="carousel-caption">
                                      <h3>New York</h3>
                                      <p>We love the Big Apple!</p>
                                    </div>
                                  </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="partner">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <ul class="list-inline text-center">
                           <li><a href="#"><img src="<?= base_url(); ?>public/img/supermicro-logo-150x80.png"></a></li>
                           <li><a href="#"><img src="<?= base_url(); ?>public/img/cpanel-logo-new-1-150x80.png"></a></li>
                           <li><a href="#"><img src="<?= base_url(); ?>public/img/whm-logo-new-150x80.png"></a></li>
                           <li><a href="#"><img src="<?= base_url(); ?>public/img/linux-logo-new-150x80.png"></a></li>
                           <li><a href="#"><img src="<?= base_url(); ?>public/img/windows-server-logo-new-150x80.png"></a></li>
                           <li><a href="#"><img src="<?= base_url(); ?>public/img/whm-logo-new-150x80.png"></a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div id="footer">
               <div class="top_footer hidden-xs">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-6">
                           <div class="support_phone">
                              <h3>Need help? 24/7 support</h3>
                              <a href="#">Click Here</a> to add your phone number
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="newsletter">
                              <div class="row">
                                 <form class="form-inline">
                                 <div class="col-lg-12">
                                    <p>Sign up to receive the latest coupons</p>
                                 </div>
                                 <div class="col-lg-9">
                                    <input type="text" name="" placeholder="Inscrivez vous dans notre newsletter" class="block_level form-control input-lg">
                                 </div>
                                 <div class="col-lg">
                                    <button class="btn btn-success btn-lg">SIGN UP</button>
                                 </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="bottom_footer">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="block">
                              <h4>Web Hosting</h4>
                              <ul class="foolist list-unstyled">
                                 <li><a href="https://www.dothostia.com/fr/hebergement-web-maroc.html">Shared Cloud Hosting</a></li>
                                 <li><a href="https://www.dothostia.com/fr/hebergement-web-revendeurs.html">Reseller Cloud Hosting</a></li>
                                 <li><a href="https://www.dothostia.com/fr/serveurs-vps-maroc.html">VPS Servers</a></li>
                                 <li><a href="https://www.dothostia.com/serveurs-dedies-maroc.html">Dedicated Servers</a></li>      
                                 <li><a href="#">Office Emails</a></li>
                                 <li><a href="#">Comparative Table</a></li> 
                              </ul>
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="block">
                              <h4>Domains and Security</h4>
                              <ul class="foolist list-unstyled">
                                 <li><a href="#">Register a Domain Name</a></li>
                                 <li><a href="#">WhoisGuard Protection</a></li>
                                 <li><a href="#">SSL Certificates</a></li>
                                 <li><a href="#">Marketing Emails</a></li>
                                 <li><a href="#">Website Builder</a></li>
                                 <li><a href="#">SiteLock Scanner</a></li>
                              </ul>
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="block">
                              <h4>About Dothostia</h4>
                              <ul class="foolist list-unstyled">
                                 <li><a href="#">About us</a></li>
                                 <li><a href="#">Our Daily Blog</a></li>
                                 <li><a href="#"> Recruitment / Internship</a></li>
                                 <li><a href="#">Our Data Centers</a></li>
                                 <li><a href="#">Payment Methods</a></li>
                                 <li><a href="#">Termes & Conditions</a></li>           
                              </ul>
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="block">
                              <p class="info">Najd 1 Résidence B3 RDC APPT N° 1 El Jadida, Maroc</p>
                              <p class="info">Mail: contact@dothostia.com</p>
                              <p class="pull-right"><img width="224" src="<?= base_url(); ?>public/img/payment-logos.png"></p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>