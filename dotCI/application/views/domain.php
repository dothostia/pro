<div class="hero_bg domain_bg">
               <div id="domain">
                  <div class="container">
                     <div class="row form-group">
                        <div class="col-lg-12">
                           <div class="block">
                              <h1>IT STARTS JUST WITH A DOMAIN NAME</h1>
                              <p>Enjoy the opportunity. New extensions. Low prices. Register your perfect domain today.</p>
                              <div class="col-lg-8 col-lg-offset-2 col-xs-offset-0">
                                 <div class="row">
                                    <div class="col-lg-9" style="padding:0;">
                                       <input type="email" class="domain_lookup col-lg-6 col-xs-12" placeholder="Enter your domain name here ...">
                                    </div>
                                    <div class="col-lg-3" style="padding:0;">
                                       <button class="btn_big"><i class="fas fa-search"></i> SEARCH</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="plans" class="block gray">
               <div class="container">
                  <div class="row">
                     <div id="vps">
                        <div class="col-lg-3">
                           <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Economique</h2>
                              <ul class="list-unstyled">
                                 <li>1 Domaine gratuit à vie</li>
                                 <li>20 Go d'espace disque</li>
                                 <li>250 Go de bande passante</li>
                                 <li>50 comptes courriel</li>
                                 <li>10 bases de données</li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>249 dh <i>&nbsp;/an</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Commander</a>
                           </div>
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Economique</h2>
                              <ul class="list-unstyled">
                                 <li>1 Domaine gratuit à vie</li>
                                 <li>20 Go d'espace disque</li>
                                 <li>250 Go de bande passante</li>
                                 <li>50 comptes courriel</li>
                                 <li>10 bases de données</li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>249 dh <i>&nbsp;/an</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Commander</a>
                           </div>
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Economique</h2>
                              <ul class="list-unstyled">
                                 <li>1 Domaine gratuit à vie</li>
                                 <li>20 Go d'espace disque</li>
                                 <li>250 Go de bande passante</li>
                                 <li>50 comptes courriel</li>
                                 <li>10 bases de données</li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>249 dh <i>&nbsp;/an</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Commander</a>
                           </div>
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Economique</h2>
                              <ul class="list-unstyled">
                                 <li>1 Domaine gratuit à vie</li>
                                 <li>20 Go d'espace disque</li>
                                 <li>250 Go de bande passante</li>
                                 <li>50 comptes courriel</li>
                                 <li>10 bases de données</li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>249 dh <i>&nbsp;/an</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Commander</a>
                           </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="cloudbg">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="text-center">
                           <h2 class="gadgets_title">Qu'est ce qu'il y a derrière le Cloud</h2>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="include_gadgets">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 Autres hébergeurs prennent des vacances. Nous ne le faisons pas, notre support travail 24/7 afin de vous garantir le success.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 Autres hébergeurs prennent des vacances. Nous ne le faisons pas, notre support travail 24/7 afin de vous garantir le success.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 Autres hébergeurs prennent des vacances. Nous ne le faisons pas, notre support travail 24/7 afin de vous garantir le success.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 Autres hébergeurs prennent des vacances. Nous ne le faisons pas, notre support travail 24/7 afin de vous garantir le success.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 Autres hébergeurs prennent des vacances. Nous ne le faisons pas, notre support travail 24/7 afin de vous garantir le success.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="gadget_item">
                           <img src="<?= base_url(); ?>public/img/support.png" alt="">
                           <div>
                              <h3 class="gadget_title">Support qui ne dort jamais</h3>
                              <p class="gadget_desc">
                                 Autres hébergeurs prennent des vacances. Nous ne le faisons pas, notre support travail 24/7 afin de vous garantir le success.
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="green_divider">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <p>
                           Bienvenue à Dothostia, nous sommes une entreprise d’hébergement Web au maroc qui offre différentes gammes d’hébergement y compris l’hébergement mutualisé, l’hébergement revendeur, les serveurs VPS et dédiés ainsi que les noms de domaine et les certificats ssl. Avec plus de 15000 clients, nous avons une réputation d’excellence et nous nous concentrons sur les services d’excellents niveaux et fiables ainsi que le soutien à la clientèle. Cela signifie que vous pouvez utiliser et profiter de nos services d’hébergement si vous êtes un freelancer à domicile, une petite, moyenne ou grande entreprise, nous sommes sûrs d’avoir une solution d’hébergement pour vous.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <div id="sections">
               <div class="block">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-6">
                           <img src="img/apps-icons.png" class="img-responsive text-center">
                        </div>
                        <div class="col-lg-6">
                           <h3>Votre Présence en Quelques Minutes</h3>
                           <p>
                              Installez votre blog WordPress, Joomla CMS, forums phpBB, Magento et plus de 300 autres applications populaires avec juste 1 seul clic. Softaculous vous permet d’installer, mettre à jour et de gérer des applications Web sans avoir besoin de connaissance technique ou préalable.
                           </p>
                           <p>
                              Il est complètement libre de l’utiliser et il n’y a aucune limite sur le nombre de scripts que vous pouvez avoir, juste en un seul clic vous aurez une présence en ligne.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="block gray">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-6">
                           <h3>Votre Présence en Quelques Minutes</h3>
                           <p>
                              Installez votre blog WordPress, Joomla CMS, forums phpBB, Magento et plus de 300 autres applications populaires avec juste 1 seul clic. Softaculous vous permet d’installer, mettre à jour et de gérer des applications Web sans avoir besoin de connaissance technique ou préalable.
                           </p>
                           <p>
                              Il est complètement libre de l’utiliser et il n’y a aucune limite sur le nombre de scripts que vous pouvez avoir, juste en un seul clic vous aurez une présence en ligne.
                           </p>
                        </div>
                        <div class="col-lg-6">
                           <div class="text-center">
                              <img src="https://my-vm.com/wp-content/uploads/2016/11/my-vm-hdd-ssd.png" class="img-responsive">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="block">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-6">
                           <img src="img/paper-lantern.gif" class="img-responsive">
                        </div>
                        <div class="col-lg-6">
                           <h3>Votre Présence en Quelques Minutes</h3>
                           <p>
                              Installez votre blog WordPress, Joomla CMS, forums phpBB, Magento et plus de 300 autres applications populaires avec juste 1 seul clic. Softaculous vous permet d’installer, mettre à jour et de gérer des applications Web sans avoir besoin de connaissance technique ou préalable.
                           </p>
                           <p>
                              Il est complètement libre de l’utiliser et il n’y a aucune limite sur le nombre de scripts que vous pouvez avoir, juste en un seul clic vous aurez une présence en ligne.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="block gray">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-6">
                           <h3>Votre Présence en Quelques Minutes</h3>
                           <p>
                              Installez votre blog WordPress, Joomla CMS, forums phpBB, Magento et plus de 300 autres applications populaires avec juste 1 seul clic. Softaculous vous permet d’installer, mettre à jour et de gérer des applications Web sans avoir besoin de connaissance technique ou préalable.
                           </p>
                           <p>
                              Il est complètement libre de l’utiliser et il n’y a aucune limite sur le nombre de scripts que vous pouvez avoir, juste en un seul clic vous aurez une présence en ligne.
                           </p>
                        </div>
                        <div class="col-lg-6">
                           <img src="img/securityhosting.png" class="img-responsive" alt="">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="block">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-12">
                           <h3>Tous nos plans incluent</h3>
                        </div>
                        <div class="col-lg-4">
                           <ul class="list-unstyled specs">
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Gestion de produit centralisée</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Soutien 24/7</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Service garanti 30 jours ou argent remis</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Configuration initiale gratuite</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Accès à notre programme de récompense</li>
                           </ul>
                        </div>
                        <div class="col-lg-4">
                           <ul class="list-unstyled specs">
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Logiciels Softaculous</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Architecture Cloud</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Accès cPanel®</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Réseau haute performance</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Sauvegardes conservées 7 jours / Amazon S3</li>
                           </ul>
                        </div>
                        <div class="col-lg-4">
                           <ul class="list-unstyled specs">
                              <li><i class="fa fa-chevron-right">&nbsp;</i> PHP 5.2 à 5.6</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Ruby 1.8 à 2.1</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Python 2.7, 3.3 & 3.4</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> MySQL (MariaDB), PostgreSQL</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Statistiques Web & journaux d’accès</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="block gray">
                  <div id="faq">
                     <div class="container">
                        <div class="row">
                           <div class="col-lg-12">
                              <div class="text-center">
                                 <h2 class="faq-title">Questions Fréquemment Posées</h2>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="faq_block">
                                 <h5><strong>Je vais être capable de gérer mon serveur via un panneau de contrôle basé sur le Web?</strong></h5>
                                 <p>
                                    Oui. Chaque serveur dédié est livré avec Web Host Manager (WHM), qui vous donne un contrôle complet sur la création et la personnalisation de votre compte, ainsi que la gestion de tous les aspects de votre serveur.
                                 </p>
                              </div>
                              <div class="faq_block">
                                 <h5><strong>Je vais être capable de gérer mon serveur via un panneau de contrôle basé sur le Web?</strong></h5>
                                 <p>
                                    Oui. Chaque serveur dédié est livré avec Web Host Manager (WHM), qui vous donne un contrôle complet sur la création et la personnalisation de votre compte, ainsi que la gestion de tous les aspects de votre serveur.
                                 </p>
                              </div>
                              <div class="faq_block">
                                 <h5><strong>Je vais être capable de gérer mon serveur via un panneau de contrôle basé sur le Web?</strong></h5>
                                 <p>
                                    Oui. Chaque serveur dédié est livré avec Web Host Manager (WHM), qui vous donne un contrôle complet sur la création et la personnalisation de votre compte, ainsi que la gestion de tous les aspects de votre serveur.
                                 </p>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="faq_block">
                                 <h5><strong>Je vais être capable de gérer mon serveur via un panneau de contrôle basé sur le Web?</strong></h5>
                                 <p>
                                    Oui. Chaque serveur dédié est livré avec Web Host Manager (WHM), qui vous donne un contrôle complet sur la création et la personnalisation de votre compte, ainsi que la gestion de tous les aspects de votre serveur.
                                 </p>
                              </div>
                              <div class="faq_block">
                                 <h5><strong>Je vais être capable de gérer mon serveur via un panneau de contrôle basé sur le Web?</strong></h5>
                                 <p>
                                    Oui. Chaque serveur dédié est livré avec Web Host Manager (WHM), qui vous donne un contrôle complet sur la création et la personnalisation de votre compte, ainsi que la gestion de tous les aspects de votre serveur.
                                 </p>
                              </div>
                              <div class="faq_block">
                                 <h5><strong>Je vais être capable de gérer mon serveur via un panneau de contrôle basé sur le Web?</strong></h5>
                                 <p>
                                    Oui. Chaque serveur dédié est livré avec Web Host Manager (WHM), qui vous donne un contrôle complet sur la création et la personnalisation de votre compte, ainsi que la gestion de tous les aspects de votre serveur.
                                 </p>
                              </div>
                           </div>
                        </div>
                  </div>
                  </div>
               </div>
               <div class="footer_hero">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-12">
                           <h2 class="footer_lead">Nous vous supportons à chaque étape</h2>
                           <p><strong>Faites de votre site plus rapide et profiter du meilleur soutien.</strong></p>
                           <a href="#" class="btn btn-success">Commencer dès maintenant</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>