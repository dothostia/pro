<div class="hero_bg reseller_bg">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <h1 class="hero_title">HÉBERGEMENT WEB MAROC - CLOUD</h1>
                        <p class="hero_lead">93% des clients marocains recommandent notre hébergement web mutualisé comme excellent !</p>
                        <a href="#" class="pull-right hero_action">Joignez-vous à notre famille</a>
                     </div>
                  </div>
               </div>
            </div>
            <div id="plans" class="block gray">
               <div class="container">
                  <div class="row">
                     <div id="reseller">
                        <div class="col-lg-3">
                        <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Light</h2>
                              <ul class="list-unstyled">
                                 <li>Disk Space<span>100GB</span></li>
                                 <li>Bandwidth <span>800GB</li>
                                 <li>Free domain name <span><i class="fa fa-check green check"></i></span></li>
                                 <li>Dedicated IP <span><i class="fa x fa-times"></i></span></li>
                                 <li>Unlimited cPanels <span><i class="fa fa-check green check"></i></span></li>
                                 <li>Free WHMCS <span><i class="fa x fa-times"></i></span></li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>249 dh <i>&nbsp;/an</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Commander</a>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-3">
                        <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Power</h2>
                              <ul class="list-unstyled">
                                 <li>Disk Space<span>150GB</span></li>
                                 <li>Bandwidth <span>1500GB</li>
                                 <li>Free domain name <span><i class="fa fa-check green check"></i></span></li>
                                 <li>Dedicated IP <span><i class="fa fa-check green check"></i></span></li>
                                 <li>Unlimited cPanels <span><i class="fa fa-check green check"></i></span></li>
                                 <li>Free WHMCS <span><i class="fa fa-check green check"></i></span></li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>249 dh <i>&nbsp;/an</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Commander</a>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-3">
                        <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Premier</h2>
                              <ul class="list-unstyled">
                                 <li>Disk Space<span>200GB</span></li>
                                 <li>Bandwidth <span>2000GB</li>
                                 <li>Free domain name <span><i class="fa fa-check green check"></i></span></li>
                                 <li>Dedicated IP <span><i class="fa fa-check green check"></i></span></li>
                                 <li>Unlimited cPanels <span><i class="fa fa-check green check"></i></span></li>
                                 <li>Free WHMCS <span><i class="fa fa-check green check"></i></span></li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>249 dh <i>&nbsp;/an</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Commander</a>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-3">
                        <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Ultimate</h2>
                              <ul class="list-unstyled">
                                 <li>Disk Space<span>250GB</span></li>
                                 <li>Bandwidth <span>2500GB</li>
                                 <li>Free domain name <span><i class="fa fa-check green check"></i></span></li>
                                 <li>Dedicated IP <span><i class="fa fa-check green check"></i></span></li>
                                 <li>Unlimited cPanels <span><i class="fa fa-check green check"></i></span></li>
                                 <li>Free WHMCS <span><i class="fa fa-check green check"></i></span></li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>249 dh <i>&nbsp;/an</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Commander</a>
                           </div>
                        </div>
                     </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="cloudbg">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-6">
                        <div class="block">
                           <h3>Rich platform for your success</h3>
                           <p class="text-justify">
                              With our reseller packages starting from 199 DH per month, you can go and benefit from a white label and a cloud platform as well as a personalized DNS according to your choice. As a reseller at Dothostia you have complete control over your clients' accounts through our cPanel / WHM (CS) control panel
                           </p>
                           <p class="text-justify">
                              We support the language you speak (in code!) PHP 5, MySQL 5, Perl, CGI, SSH, SSI, HTML5 you will be able to offer your customers the same features of shared web hosting that we offer, features such as e-mail accounts, Mysql databases, SEO tools like Attaracta SEO and many more options.
                           </p>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="block">
                           <div class="row">
                              <div class="col-lg-5">
                                 <img class="img-responsive" src="https://cdn-images-1.medium.com/max/1533/1*gW7kbOcSIwMosurOt2uRXw.png">
                              </div>
                              <div class="col-lg-7">
                                 <strong>Free Billing Software</strong>
                                 <p>
                                    The WHMCS reseller interface allows you to automate the most important processes such as suspension, termination, bill generation and registration of domain names etc.
                                 </p>
                              </div>
                              <div class="clearfix"></div>
                              <div class="col-lg-5">
                                 <img class="img-responsive" src="https://cdn-images-1.medium.com/max/1533/1*gW7kbOcSIwMosurOt2uRXw.png">
                              </div>
                              <div class="col-lg-7">
                                 <strong>WHM reseller interface</strong>
                                 <p>
                                    Web Host Manager is the backend part of Resellers, where you add, edit, and delete hosting accounts. You also have the opportunity to update your plans.
                                 </p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="sections">
               <div class="block gray">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-6">
                           <img src="img/apps-icons.png" class="img-responsive text-center">
                        </div>
                        <div class="col-lg-6">
                           <h3>Opportunities to earn additional income</h3>
                           <p>
                              The web hosting industry is very competitive, which is why you should be able to offer a variety of complementary services and options. Dothostia's eco-system program is powerful enough to help startups earn a lot more and expand into the web hosting industry market.
                           </p>
                           <p>
                              You can take advantage of discounts on sales of domain names, as well as SSL certificates according to your chosen reseller plan.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="block">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-6">
                           <h3>Concentrate on your business, we do the rest!</h3>
                           <p>
                              Our team is yours. If you really want to make money or even start your own web hosting company full time, go buy a Dothostia reseller plan. If you are a young website developer, do not be afraid! Dothostia accompanies you every moment thanks to its 24/7 service. Contact us by sending a ticket to support@dothostia.com or via LiveChat.
                           </p>
                           <p>
                              While we guarantee you total satisfaction, all our web hosting services including reseller hosting are backed by our guarantee.
                           </p>
                        </div>
                        <div class="col-lg-6">
                           <div class="text-center">
                              <img src="https://my-vm.com/wp-content/uploads/2016/11/my-vm-hdd-ssd.png" class="img-responsive">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="block gray">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-6">
                           <img src="img/paper-lantern.gif" class="img-responsive">
                        </div>
                        <div class="col-lg-6">
                           <h3>Your customers are always safe</h3>
                           <p>
                              Unlike other hosts, we have a perfect marriage with Amazon S3, we make a backup copy of all data from the server every 24 hours to ensure peace of mind and success on the web. All of our hosting servers are deployed with a RAID 10 storage array providing redundancy in case of hard drive failure.
                           </p>
                           <p>
                              We also use CloudFlare's solutions for much more security and quick access to a CDN. We also use CodeGuard which is the best site for cloud backups.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="block">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-12">
                           <h3>All our plans include</h3>
                        </div>
                        <div class="col-lg-4">
                           <ul class="list-unstyled specs">
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Centralized Product Management</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> 24/7 support</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Guaranteed service 30 days or money back</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Free initial configuration</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Access to our reward program</li>
                           </ul>
                        </div>
                        <div class="col-lg-4">
                           <ul class="list-unstyled specs">
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Softaculous software</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Cloud Architecture</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> CPanel® access</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> High performance network</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Saved backups 7 days / Amazon S3</li>
                           </ul>
                        </div>
                        <div class="col-lg-4">
                           <ul class="list-unstyled specs">
                              <li><i class="fa fa-chevron-right">&nbsp;</i> PHP 5.2 à 5.6</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Ruby 1.8 à 2.1</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Python 2.7, 3.3 & 3.4</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> MySQL (MariaDB), PostgreSQL</li>
                              <li><i class="fa fa-chevron-right">&nbsp;</i> Web Statistics & Access Logs</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="block gray">
                  <div id="faq">
                     <div class="container">
                        <div class="row">
                           <div class="col-lg-12">
                              <div class="text-center">
                                 <h2 class="faq-title">Questions Fréquemment Posées</h2>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="faq_block">
                                 <h5><strong>What is Reseller Hosting?</strong></h5>
                                 <p>
                                    Reseller hosting is simply the ability to resell shared hosting, domain names, and SSL certificates or other options provided by our web hosting service to your customers as if they were yours under the name of your brand name at any price you choose. This opportunity allows you to start your own web hosting business or complete your development or design services.
                                 </p>
                              </div>
                              <div class="faq_block">
                                 <h5><strong>I have to be an expert to benefit from reseller hosting?</strong></h5>
                                 <p>
                                    Not at all, concentrate on building your business and we maintain the network infrastructure, we deal with hardware installation, maintenance, configuration and server security. Hardware problems that may arise are our responsibility, not yours. Dothostia is recognized in the Moroccan web hosting sector as the leader in support and superior quality of services.
                                 </p>
                              </div>
                              <div class="faq_block">
                                 <h5><strong>Do I need to have a VPS or a dedicated server to use Reseller hosting?</strong></h5>
                                 <p>
                                    Dothostia is proud to make the reseller web hosting program accessible to everyone.
                                 </p>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="faq_block">
                                 <h5><strong>My customers will know that I am a Reseller?</strong></h5>
                                 <p>
                                    No, we have a White Label policy, you can start your own web hosting company backed by us by offering you servers with anonymous DNS and that's what makes the difference between Dothostia and the other hosting presidents Moroccan web.
                                 </p>
                              </div>
                              <div class="faq_block">
                                 <h5><strong>Can I re-sell other services?</strong></h5>
                                 <p>
                                    
Yes, you can re-sell domain names, ssl certificates and other services, We also offer the opportunity to use our API for a third-party platform, it's completely free for all our resellers.
                                 </p>
                              </div>
                              <div class="faq_block">
                                 <h5><strong>How much bandwidth can I give to my clients?</strong></h5>
                                 <p>
                                    We offer our resellers a high bandwidth so that they can then pass it on to their customers as long as they comply with our Terms and Conditions of Acceptable Use.
                                 </p>
                              </div>
                              <div class="faq_block">
                     <h5><strong>How long does it take to set up my account?</strong></h5>
                     <p>
                        After purchasing your hosting package, your account begins the installation process. The estimated time for each type of accommodation is as follows:
                        <ul class="list-unstyled">
                           <li>
                           <i class="fa green fa-chevron-circle-down"></i> <a href="https://www.dothostia.com/fr/hebergement-web-maroc.html">Shared Web Hosting</a> ou <a href="https://www.dothostia.com/fr/hebergement-web-revendeurs.html">Reseller Hosting</a> : 1H Maximum</li>
                           <li><i class="fa green fa-chevron-circle-down"></i> <a href="https://www.dothostia.com/serveurs-vps-maroc.html">cPanel VPS Server</a> : 3H Maximum</li>
                           <li><i class="fa green fa-chevron-circle-down"></i> <a href="https://www.dothostia.com/serveurs-dedies-maroc.html">Dedicated Server</a> : 48h Maximum</li>
                           <li><i class="fa green fa-chevron-circle-down"></i> <a href="https://www.dothostia.com/fr/certificat-ssl-maroc.html">SSL Certificate</a> : 30 minutes Maximum</li>
                        </ul>
                     </p>
                  </div>
                           </div>
                        </div>
                  </div>
                  </div>
               </div>
               <div class="footer_hero">
                  <div class="container">
                     <div class="row">
                        <div class="col-lg-12">
                           <h2 class="footer_lead">Nous vous supportons à chaque étape</h2>
                           <p><strong>Faites de votre site plus rapide et profiter du meilleur soutien.</strong></p>
                           <a href="#" class="btn btn-success">Commencer dès maintenant</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div