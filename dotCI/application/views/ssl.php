<div class="hero_bg ssl_bg">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <h1 class="hero_title">SSL CERTIFICATES - CANADA</h1>
                        <p class="hero_lead">Protégez vos données sensibles et celles de vos visiteurs et faîtes leurs sentir sécurisés !</p>
                        <a href="#" class="pull-right hero_action">Joignez-vous à notre famille</a>
                     </div>
                  </div>
               </div>
            </div>
            <div id="plans" class="block gray">
               <div class="container">
                  <div class="row">
                     <div id="vps">
                        <div class="col-lg-4">
                           <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Rapid SSL</h2>
                              <ul class="list-unstyled">
                                 <li>1 Domaine gratuit à vie</li>
                                 <li>20 Go d'espace disque</li>
                                 <li>250 Go de bande passante</li>
                                 <li>50 comptes courriel</li>
                                 <li>10 bases de données</li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>249 dh <i>&nbsp;/an</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Commander</a>
                           </div>
                           </div>
                        </div>
                        <div class="col-lg-4">
                           <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">Positive SSL</h2>
                              <ul class="list-unstyled">
                                 <li>1 Domaine gratuit à vie</li>
                                 <li>20 Go d'espace disque</li>
                                 <li>250 Go de bande passante</li>
                                 <li>50 comptes courriel</li>
                                 <li>10 bases de données</li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>249 dh <i>&nbsp;/an</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Commander</a>
                           </div>
                           </div>
                        </div>
                        <div class="col-lg-4">
                           <div class="box_shadow">
                           <div class="box_shadow_desc">
                              <h2 class="plan_name">GeoTrust Rapid SSL</h2>
                              <ul class="list-unstyled">
                                 <li>1 Domaine gratuit à vie</li>
                                 <li>20 Go d'espace disque</li>
                                 <li>250 Go de bande passante</li>
                                 <li>50 comptes courriel</li>
                                 <li>10 bases de données</li>
                              </ul>
                           </div>
                           <div class="price">
                              <del>299 DH</del>
                              <p class="current_price"><strong>249 dh <i>&nbsp;/an</i></strong></p>
                              <a href="#" class="btn btn-success" style="width:100%;">Commander</a>
                           </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="description">
               <div class="container">
                  <div class="block">
                     <div class="row">
                        <div class="col-lg-8">
                           <h3>Securing your site has never been easy</h3>
                           <p>
                              Your digital business or e-commerce site is based on trust. Clients will not fill out a form or shopping cart until they know that their personal information will be safe. Dothostia's SSL Certificates are affordable to secure your site, increase your sales, and encrypt all incoming and outgoing confidential information such as online payment information.
                           </p>
                           <p>
                              Dothostia offers the Comodo SSL certificate, which is rich in features:
                           </p>
                           <p>
                              <ul class="list-unstyled">
                                 <li><i class="fa green fa-check"></i> Compatible avec 99% des navigateurs y compris les périphériques mobiles</li>
                                 <li><i class="fa green fa-check"></i> Cryptage de 256-bit ainsi que 2048 bits root</li>
                                 <li><i class="fa green fa-check"></i> L’authentification automatique et l’émission en quelques minutes</li>
                                 <li><i class="fa green fa-check"></i> Une garantie arrive jusqu’à 10.000 $</li>
                              </ul>
                           </p>
                        </div>
                        <div class="col-lg-4">
                           <p>
                              <img src="https://www.dothostia.com/wp-content/uploads/2016/07/browser-ssl-2.png">
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="green_divider">
               <div class="container">
                  <div class="row">
                     <div class="block">
                        <div class="col-lg-10">
                           <h2 class="nomargin">Transfer Your SSL Certificate At Dothostia right away</h2>
                        </div>
                        <div class="col-lg-2">
                           <a href="" class="pull-right btn btn-light action">Let's do it together...</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="sections">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-6">
                        <div class="block">
                           <iframe src="https://www.youtube.com/embed/EUAYCwCgvPg?feature=oembed" gesture="media" allow="encrypted-media" allowfullscreen="" width="500" height="281" frameborder="0"></iframe>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="block">
                           <h3 class="nomargin">Protect your customers, increase your ranking on Google</h3>
                        <p>
                           Back in August 2014, Google announced that it will use the HTTPS protocile as a ranking signal "ranking signal" in their search algorithm, that is to say a parameter that the search engine will take into account when ranking pages on search results.
                        </p>
                        <p>
                           Be demanding, do not give the attackers an opportunity. Get SSL encryption and improve your business around the world and your ranking on the famous search engines.
                        </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>