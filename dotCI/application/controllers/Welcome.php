<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public $data = array();

	public function index(){
		$this->data['page'] = 'index';
		$this->data['title'] = 'Web Hosting Canada';
		$this->data['desc'] = 'Leader web hosting in Canada. Start your website with dothostia & get a free domain name and the best 24/7 support on all our cloud hosting plans.';
		$this->load->view('layout', $this->data);
	}

	public function shared(){
		$this->data['page'] = 'shared';
		$this->data['title'] = 'Web Hosting Plans width SSDs';
		$this->data['desc'] = 'With shared web hosting from dothostia canada, we make it easy for your success with an affordable price. Visit us today to discover all of our affordable hosting plans!';
		$this->load->view('layout', $this->data);
	}

	public function reseller(){
		$this->data['page'] = 'reseller';
		$this->data['title'] = 'Reseller Hosting Plans width SSDs';
		$this->data['desc'] = 'Start selling Canadian SSDs web hosting and top-level Domain names. We offer free SSL and domain name, WHMCS, and namecheap free account, transfers, 24/7 customer support.';
		$this->load->view('layout', $this->data);
	}

	public function vps(){
		$this->data['page'] = 'vps';
		$this->data['title'] = 'Cloud VPS Hosting';
		$this->data['desc'] = 'Dothostia is the leader web hosting provider of fast and secure SSDs VPS hosting based on Canada with an affordable price. Discover why millions of sites trust us for managed VPS Linux web hosting needs!';
		$this->load->view('layout', $this->data);
	}

	public function dedicated(){
		$this->data['page'] = 'dedicated';
		$this->data['title'] = 'Dedicated Server Hosting';
		$this->data['desc'] = 'Linux Dedicated Server Hosting plans, now with free solid-state drives. Get the highest levels of speed, security, uptime and cPanel/WHM';
		$this->load->view('layout', $this->data);
	}

	public function ssl(){
		$this->data['page'] = 'ssl';
		$this->data['title'] = 'Affordable SSL Certificates';
		$this->data['desc'] = 'SSL Certificates are necessary for your website security. Dothostia brings you affordable SSL Certificates like Comodo, GeoTrust...';
		$this->load->view('layout', $this->data);
	}

	public function domain(){
		$this->data['page'] = 'domain';
		$this->data['title'] = 'Register your Domain name';
		$this->data['desc'] = 'Your Domain Name is the starting point of your online business. Simple domain search & domain name registration with Dothostia Canada.';
		$this->load->view('layout', $this->data);
	}


}
